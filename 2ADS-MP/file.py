from main import getMatrice_init
from glouton import *


def top_down(liste, line, outline, track, trackLine, a=0, b=0):
    if len(liste) > a and len(liste) > b:
        if track[a][b] != -1:
            return track[a][b], trackLine[a][b]
        elif liste[a][b] < 0:
            return 9999999999999999999, line[b]
        else:
            nextLine, outlineL = top_down(liste, line, outline, track, trackLine, b+1, b+1)
            nextNumber, outlineN = top_down(liste, line, outline, track, trackLine, a, b+1)
            if (liste[a][b] + nextLine) < nextNumber:
                track[a][b] = liste[a][b] + nextLine
                outline = line[b] + "\n" + outlineL
            else:
                track[a][b] = nextNumber
                outline += line[b] + " " + outlineN
            trackLine[a][b] = outline
            return track[a][b], outline
    else:
        return 0, ""


def top_down_main(liste, line):
    track = [[-1] * len(liste) for a in range(len(liste))]
    trackLine = [[""] * len(liste) for a in range(len(liste))]

    return top_down(liste, line, "", track, trackLine)


def main():
    # ligne à tratier
    # line = "i am working very late at SUPINFO International University for finishing this exam"

    # line = "sss uu pp iiiii"

    file = open("before.txt", "r")
    line = file.readline()
    file.close()

    print("\nMain with file :", line)

    # on la récupère sous forme de liste
    wordList = line.split()
    # on recupérère également la taille de chaque mots
    lenWordList = [len(wordList[i]) for i in range(len(wordList))]

    M = 20
    # on execute la function principale
    cost = getMatrice_init(wordList, lenWordList, M)

    final, outline = top_down_main(cost, wordList)
    print("\nTop down :\n", outline, "\nMin :", final, sep='')


    # contre exemple du glouton avec la chaine "sss uu pp iiiii"
    # car le glouton est performant et rapide mais sa réponse est approximative
    outline = glouton(wordList, M)
    print("\nGlouton :\n", outline, sep='')

    # Save out file
    file = open("after.txt", "w")
    file.write(outline)


main()
