
def naif_recursif_min(liste, a=0, b=0):
    if len(liste) > a and len(liste) > b:
        if liste[a][b] < 0:
            return 9999999999999999999
        else:
            nextLine = naif_recursif_min(liste, b+1, b+1)
            nextNumber = naif_recursif_min(liste, a, b+1)

            if (liste[a][b] + nextLine) < nextNumber:
                return liste[a][b] + nextLine
            else:
                return nextNumber
    else:
        return 0
    #ESTIMATION DE LA COMPLEXITER ALGORITHMIQUE 2(n-1)!

    """
    en partant de tout en haut de la pyramide n-1 possibilitées par la droite et n-1 possiilitées dans la diagonale soit : (n-1)+(n-1)
    la matrice de 0 est l'ensemble des parcours possibles de l'algorithme
    le nombre de 0 correspond aux nombres de termes de la liste
    0-0-0-0-0-0
      0-0-0-0-0
        0-0-0-0
          0-0-0
            0-0
              0
    puis chaque chemin a n-2 possibiltées par la droite et en diagonal et n-2 dans la diagonale soit : (n-1)(n-2+(n-1)(n-2)
      0-0-0-0-0
        0-0-0-0
          0-0-0
            0-0
              0

    ect soit : (n-1)(n-2(n-3)...(n-(n-1))+(n-1)(n-2)(n-3)....(n-(n-1)) = (n-1)! +(n-1)! = 2 (n-1)!
    """


def top_down(liste, track, a=0, b=0):
    if len(liste) > a and len(liste) > b:
        if track[a][b] != -1:
            return track[a][b]
        elif liste[a][b] < 0:
            return 9999999999999999999
        else:
            nextLine = top_down(liste, track, b+1, b+1)
            nextNumber = top_down(liste, track, a, b+1)
            if (liste[a][b] + nextLine) < nextNumber:
                track[a][b] = liste[a][b] + nextLine
            else:
                track[a][b] = nextNumber

            return track[a][b]
    else:
        return 0


def top_down_main(liste):
    track = [[-1] * len(liste) for a in range(len(liste))]

    return top_down(liste, track)


def findMinColonne(liste, at, col=-1):
    tempL = []
    for l in liste[0:at]:
        a = l[col]
        if a >= 0:
            tempL.append(a)
    return min(tempL)


def bottom_up(liste):
    track = [[0] * len(liste) for a in range(len(liste))]

    for x in range(len(liste)):
        temp = 0
        for y in range(len(liste[x])):
            if liste[x][y] < 0:
                track[x][y] = 9999999999999
                continue
            temp += 1

            try:
                previousNumber = track[x][y-1]
                if previousNumber < 0:
                    raise Exception
            except Exception:
                previousNumber = 9999999999

            try:
                previousLine = findMinColonne(track, x, y-temp)
                if previousLine < 0:
                    raise Exception
            except Exception:
                previousLine = 9999999999

            if (liste[x][y] + previousLine) < previousNumber:
                track[x][y] = liste[x][y] + previousLine
            else:
                track[x][y] = liste[x][y]

    return findMinColonne(track, len(track))

    #ESTIMATION DE LA COMPLEXITER ALGORITHMIQUE
    """
        la matrice de 0 est l'ensemble des parcours possibles de l'algorithme
        et les parcours possible représente l'ensemble des coefficient de la matrice soit la somme des premiers entiers jusqu'à n soit : Somme de k=1 a n de k = (n(n+1))/2 = (n^2 + n) /2


        0-0-0-0-0-0
          0-0-0-0-0
            0-0-0-0
              0-0-0
                0-0
                  0

    """