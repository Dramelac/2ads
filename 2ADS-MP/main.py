from find import *


def getMatrice(lenWordList, M, cost, i=0, j=0):
    # condition de sortie de la recursion
    if i == len(lenWordList):
        return cost
    # ligne suivante
    elif j == len(lenWordList):
        return getMatrice(lenWordList, M, cost, i + 1, i + 1)
    else:
        # calcul du cout
        wordCount = 0
        for x in range(i, j+1):
            wordCount += lenWordList[x]

        costTemp = pow(M - wordCount - (j - i), 3)
        if j < len(lenWordList) - 1 or costTemp < 0:
            # print('(', M, "-", wordCount, '- (', j, '-', i, ') ) ^ 3 = ', pow(M - wordCount - (j - i), 3))
            cost[i][j] = costTemp
        # cas de la dernière ligne (non calcul du cout supplémentaire)
        else:
            cost[i][j] = 0
        # colonne suivante
        return getMatrice(lenWordList, M, cost, i, j + 1)


def getMatrice_init(wordList, lenWordList, M):
    # on initialise les listes
    cost = [[-1] * len(wordList) for i in range(len(wordList))]

    print("M :", M)

    # calcule de la matrice cost
    cost = getMatrice(lenWordList, M, cost)

    """
    # affichage debug de la matrice
    for line in cost:
        for number in line:
            print(number, end=' ')
        print()
    """

    return cost


def main():
    # ligne à traiter
    # line = "i am working very late at SUPINFO International University for finishing this exam"
    line = "sss uu pp iiiii"

    print("Main with line :", line)

    # on la récupère sous forme de liste
    wordList = line.split()
    # on recupérère également la taille de chaque mots
    lenWordList = [len(wordList[i]) for i in range(len(wordList))]

    M = max(lenWordList) + 1
    # M = 20

    # on execute la function principale
    cost = getMatrice_init(wordList, lenWordList, M)

    final = naif_recursif_min(cost)
    print("naif :", final)

    final = top_down_main(cost)
    print("top down :", final)

    final = bottom_up(cost)
    print("bottom up :", final)


main()
