

def glouton(wordLine, M):
    output = ""
    lineActual = 0
    for word in wordLine:
        costTemp = len(word)
        if lineActual + costTemp <= M:
            output += word + " "
            lineActual += costTemp + 1
        else:
            output += "\n" + word + " "
            lineActual = costTemp + 1

    return output
